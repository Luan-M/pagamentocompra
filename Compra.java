package domain;

import java.util.Scanner;

public class Compra {
	
	static final Double PIX_DESCONTO = 0.07;
	static final Double DINHEIRO_DESCONTO = 0.07;
	
	static final Double JUROS = 0.1;
		
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			
		System.out.print("Qual o valor da compra: ");
		Double valorCompra = scanner.nextDouble();
			
		System.out.println("Qual a forma do pagamento?");
		System.out.print("1 - Pix | 2 - Dinheiro | 3 - Cartão: ");
		Integer formaPagamento = scanner.nextInt();
			
		System.out.println();
			
		if(formaPagamento.equals(1)) {
			Double pix = valorCompra * PIX_DESCONTO;
			valorCompra -= pix;
			System.out.println("Pix tem 7% de desconto.");
			System.out.println("Valor: R$ " + valorCompra);
				
		} else if(formaPagamento.equals(2)) {
			Double dinheiro = valorCompra * DINHEIRO_DESCONTO;
			valorCompra -= dinheiro;
			System.out.println("Dinheiro tem 7% de desconto.");
			System.out.println("Valor: R$ " + valorCompra);
				
		} else if(formaPagamento.equals(3)) {
			System.out.println("Qual o tipo do cartão?");
			System.out.print("1 - Débito | 2 - Crédito: ");
			Integer tipoCartao = scanner.nextInt();
				
			if(tipoCartao.equals(1)) {
				System.out.println("Cartão de débito o valor é inteiro.");
				System.out.println("Valor: R$ " + valorCompra);
					
			} else if(tipoCartao.equals(2)) {
				Double juros = valorCompra * JUROS;
				valorCompra += juros;
				System.out.println("Cartão de débito tem um juros de 10%.");
				System.out.println("Valor: R$ " + valorCompra);
			} else {
				System.out.println("Você só pode escolher 1 para débito ou 2 para crédito.");
				System.out.println("Recomece.");
			}
				
		} else {
			System.out.println("Você só pode escolher 1 para pix, 2 para dinheiro ou 3 para cartão.");
			System.out.println("Recomece, por favor.");
		}
			
			
		scanner.close();
	}

}

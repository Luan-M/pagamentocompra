Dependendo da forma que o cliente escolher, pode ter desconto, juros ou o valor normal.
Se o cliente escolher o pix ou pagar em dinheiro, vai ter 7% de desconto.
Se o cliente escolher pagar no cartão no débito vai pagar o valor normal.
Se o cliente escolher pagar no cartão no crédito vai ter que pagar 10% de juros.
